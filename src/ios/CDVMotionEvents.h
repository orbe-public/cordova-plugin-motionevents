// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>
#import <CoreMotion/CMMotionActivityManager.h>

@interface CDVMotionEvents : CDVPlugin
{
    // whether the device supports the heading service
    BOOL isHeadingAvailable;
    // whether the app is currently listening for motion
    BOOL isListening;
    // whether the app is currently getting data for the first time
    BOOL isFirstUpdate;
    // to compute the difference while using a reference frame
    double initialZRot;
}

// Getting the availability of device motion services
- (void)getServicesAvailability:(CDVInvokedUrlCommand*)command;

// Start or stop device-motion updates
- (void)start:(CDVInvokedUrlCommand*)command;
- (void)stop:(CDVInvokedUrlCommand*)command;

@end
