// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import "CDVMotionEvents.h"
#import <Cordova/CDVPlugin.h>
#import <CoreMotion/CMMotionManager.h>
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CLHeading.h>


#define rad2deg(x) ( x * (180/M_PI) )

static const double kGravity = 9.80665;
static const double kMotionUpdateInterval = 1.0f / 60.0f;


@interface CDVMotionEvents()

@end

@implementation CDVMotionEvents
{
    @private  NSMutableArray *listeners;
    @private CMMotionManager *motionManager;
    @private CLLocationManager *locationManager;
    
    @private double period;  // the device-motion service update period (in seconds).
    @private NSDictionary *currentData; // the current device-motion data.
}

/**
 * Initialize manager and properties.
 */
- (void)pluginInitialize
{
    listeners = [[NSMutableArray alloc]init];
    motionManager = [[CMMotionManager alloc] init];
    locationManager = [[CLLocationManager alloc] init];
    isHeadingAvailable = locationManager ? [CLLocationManager headingAvailable] : NO;
    
    currentData = @{};
}

/**
 * Returns whether the device-motion services are available or not on the device.
 *
 * The Cordova plugin will return an object containing the availability of motion, of the
 * accelerometer and the gyroscope on success or an error message if motion is not supported by this
 * device.
 *
 * @param command the Cordova plugin request.
 */
- (void)getServicesAvailability:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    if (motionManager) {
        NSDictionary* services = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithBool:motionManager.isDeviceMotionAvailable], @"motion",
                                  [NSNumber numberWithBool:motionManager.isAccelerometerAvailable], @"accelerometer",
                                  [NSNumber numberWithBool:motionManager.isGyroAvailable], @"gyroscope",
                                  nil];
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                     messageAsDictionary:services];
    }
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Motion is not supported by this device."];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

/**
 * Registers a new listener (if necessary) attached to the given command.
 *
 * @param command the Cordova plugin request.
 * @param period the listener callback period
 * @return whether the listener already exists or not.
 */
- (BOOL)registerListener:(CDVInvokedUrlCommand*)command withPeriod:(NSNumber*)period
{
    NSString* uuid = [command argumentAtIndex:0 withDefault:nil];
    
    // search if exists
    for (NSDictionary* listener in listeners) {
        if ([uuid isEqual:listener[@"id"]]) return NO;
    }
    
    
    // temporise data outputs
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:[period doubleValue]
                                                      target:self
                                                    selector:@selector(sendMotionEventData:)
                                                    userInfo:command.callbackId
                                                     repeats:YES];
    NSDictionary* listener = @{
        @"id": uuid,
        @"period": period,
        @"callback": command.callbackId,
        @"timer": timer
    };
    [listeners addObject:listener];
    
    return YES;
}

/**
 * Unregisters a listener attached to the given command.
 *
 * @param command the Cordova plugin request.
 * @retunr  whether the list of registered listeners is empty or not.
 */
- (BOOL)unregisterListener:(CDVInvokedUrlCommand*)command
{
    NSString* uuid = [command argumentAtIndex:0 withDefault:nil];
    
    // search, stop timer and mark as deleted
    // or retrieve the minimum period
    period = 0;
    NSDictionary* toberemoved = nil;
    for (NSDictionary* listener in listeners) {
        if ([uuid isEqual:listener[@"id"]]) {
            [(NSTimer*)listener[@"timer"] invalidate];
            toberemoved = listener;
        }
        else {
            NSNumber* p = (NSNumber*)listener[@"period"];
            if ([p doubleValue] < period || period == 0) period = [p doubleValue];
        }
    }
    
    if (toberemoved != nil ) [listeners removeObject:toberemoved];
    if ([listeners count]==0) return YES;
    
    // set the new device-motion service period
    if ([motionManager isDeviceMotionAvailable] == YES)
        motionManager.deviceMotionUpdateInterval = period;
    if ([motionManager isAccelerometerAvailable] == YES)
        motionManager.accelerometerUpdateInterval = period;
    
    return NO;
}

/**
 * Stores the given device-motion data to sends it later to all listeners.
 * @param data the device-motion data to be sent.
 */
- (void)storeMotionEventData:(NSDictionary*)data
{
    currentData = data;
}

/**
 * Send the current device-motion data to the given scheduler callback method.
 * @param listener the callback timer attached to a listener.
 */
- (void)sendMotionEventData:(NSTimer*)scheduler
{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                  messageAsDictionary:currentData];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:[scheduler userInfo]];
}

/**
 * Starts device-motion services.
 *
 * The Cordova plugin will return the device-motion data on success or an error message on failure.
 * based on WebKit core motion manager
 * https://github.com/WebKit/WebKit/blob/bc14815b8d3c3280deefd530dbca94d880fdc9ca/Source/WebCore/platform/ios/WebCoreMotionManager.mm
 * and DeviceMotion relative to world
 * https://stackoverflow.com/questions/7950096/devicemotion-relative-to-world-multiplybyinverseofattitude
 *
 * @param command the Cordova plugin request.
 */
- (void)start:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    if (motionManager) {
        isListening = YES;
        isFirstUpdate = YES;
        
        NSDictionary* options = [command argumentAtIndex:1 withDefault:nil];
        NSNumber* ms = [options valueForKey:@"period"];
        
        double interval = ms==nil ? kMotionUpdateInterval : [ms doubleValue] / 1000.f;
        
        BOOL registered = [self registerListener:command withPeriod:[NSNumber numberWithDouble:interval]];
        if (registered) {
            if (interval < period || period == 0) period = interval;
            
            // The device-motion service is available if a device has both an accelerometer and a gyroscope.
            if ([motionManager isDeviceMotionAvailable] == YES) {
                motionManager.deviceMotionUpdateInterval = period;
                if ([motionManager isDeviceMotionActive] == NO) {
                    if (isHeadingAvailable == YES) [locationManager startUpdatingHeading];
                    CMAttitudeReferenceFrame frame = CMAttitudeReferenceFrameXTrueNorthZVertical;
                    if ([[UIDevice currentDevice].model isEqualToString:@"iPod touch"]) frame = CMAttitudeReferenceFrameXArbitraryZVertical;
                    [motionManager startDeviceMotionUpdatesUsingReferenceFrame:frame toQueue:[NSOperationQueue mainQueue] withHandler:^(CMDeviceMotion *motion, NSError *error)
                     {
                        CMAttitude* attitude = motion.attitude;
                        CMRotationRate rotationRate = motion.rotationRate;
                        CMAcceleration acceleration  = motion.userAcceleration;
                        CMAcceleration gravity = motion.gravity;
                    
                        
                        
                        // Compose the raw motion data to an intermediate ZXY-based 3x3 rotation
                        // matrix (R) where [z=attitude.yaw, x=attitude.pitch, y=attitude.roll]
                        // in the form:
                        //
                        //   /  R[0]   R[1]   R[2]  \
                        //   |  R[3]   R[4]   R[5]  |
                        //   \  R[6]   R[7]   R[8]  /
                        
                        double cX = cos(attitude.pitch);
                        double cY = cos(attitude.roll);
                        double cZ = cos(attitude.yaw);
                        double sX = sin(attitude.pitch);
                        double sY = sin(attitude.roll);
                        double sZ = sin(attitude.yaw);
                        
                        double R[] = {
                            cZ * cY - sZ * sX * sY,
                            - cX * sZ,
                            cY * sZ * sX + cZ * sY,
                            cY * sZ + cZ * sX * sY,
                            cZ * cX,
                            sZ * sY - cZ * cY * sX,
                            - cX * sY,
                            sX,
                            cX * cY
                        };
                        
                        // Compute correct, normalized values for DeviceOrientation from
                        // rotation matrix (R) according to the angle conventions defined in the
                        // W3C DeviceOrientation specification.
                        
                        double zRot;
                        double xRot;
                        double yRot;
                        
                        if (R[8] > 0) {
                            zRot = atan2(-R[1], R[4]);
                            xRot = asin(R[7]);
                            yRot = atan2(-R[6], R[8]);
                        } else if (R[8] < 0) {
                            zRot = atan2(R[1], -R[4]);
                            xRot = -asin(R[7]);
                            xRot += (xRot >= 0) ? -M_PI : M_PI;
                            yRot = atan2(R[6], -R[8]);
                        } else {
                            if (R[6] > 0) {
                                zRot = atan2(-R[1], R[4]);
                                xRot = asin(R[7]);
                                yRot = -M_PI_2;
                            } else if (R[6] < 0) {
                                zRot = atan2(R[1], -R[4]);
                                xRot = -asin(R[7]);
                                xRot += (xRot >= 0) ? -M_PI : M_PI;
                                yRot = -M_PI_2;
                            } else {
                                zRot = atan2(R[3], R[0]);
                                xRot = (R[7] > 0) ? M_PI_2 : -M_PI_2;
                                yRot = 0;
                            }
                        }
                        
                        // register once or make the difference with the initial zRot value
                        if (self->isFirstUpdate) {
                            self->initialZRot = zRot;
                            self->isFirstUpdate = NO;
                        }
                        else zRot -= self->initialZRot;
                        
                        // rotation around the Z axis (pointing up. normalized to [0, 360] deg).
                        double alpha = rad2deg((zRot > 0 ? zRot : (M_PI * 2 + zRot)));
                        // rotation around the X axis (top to bottom).
                        double beta  = rad2deg(xRot);
                        // rotation around the Y axis (side to side).
                        double gamma = rad2deg(yRot);
                        
                        // get heading from the reference frame  or use the one given by the location manager
                        double heading = motion.heading;
                        if (heading == -1 && self->isHeadingAvailable)
                            heading = [[self->locationManager heading] magneticHeading];
                        
                        
                        // transform acceleration to reference frame:
                        // North-South/East-West acceleration rather than left-right/forward-backward
                        CMRotationMatrix rotMatrix = attitude.rotationMatrix;
                        CMAcceleration worldAccel;
                        
                        worldAccel.x = acceleration.x*rotMatrix.m11 + acceleration.y*rotMatrix.m12 + acceleration.z*rotMatrix.m13;
                        worldAccel.y = acceleration.x*rotMatrix.m21 + acceleration.y*rotMatrix.m22 + acceleration.z*rotMatrix.m23;
                        worldAccel.z = acceleration.x*rotMatrix.m31 + acceleration.y*rotMatrix.m32 + acceleration.z*rotMatrix.m33;
                        
                        
                        // pack all
                        NSDictionary* result = @{
                            @"acceleration": @{
                                @"local": @{
                                    @"x": [NSNumber numberWithDouble:acceleration.x * kGravity],
                                    @"y": [NSNumber numberWithDouble:acceleration.y * kGravity],
                                    @"z": [NSNumber numberWithDouble:acceleration.z * kGravity],
                                },
                                @"world": @{
                                    @"x": [NSNumber numberWithDouble:worldAccel.x * kGravity],
                                    @"y": [NSNumber numberWithDouble:worldAccel.y * kGravity],
                                    @"z": [NSNumber numberWithDouble:worldAccel.z * kGravity],
                                },
                            },
                            @"accelerationIncludingGravity": @{
                                @"local": @{
                                    @"x": [NSNumber numberWithDouble:(acceleration.x + gravity.x) * kGravity],
                                    @"y": [NSNumber numberWithDouble:(acceleration.y + gravity.y) * kGravity],
                                    @"z": [NSNumber numberWithDouble:(acceleration.z + gravity.z) * kGravity],
                                },
                                @"world": @{
                                    @"x": [NSNumber numberWithDouble:(worldAccel.x + gravity.x) * kGravity],
                                    @"y": [NSNumber numberWithDouble:(worldAccel.y + gravity.y) * kGravity],
                                    @"z": [NSNumber numberWithDouble:(worldAccel.z + gravity.z) * kGravity],
                                }
                            }
                            ,
                            @"rotationRate": @{
                                @"alpha": [NSNumber numberWithDouble:rad2deg(rotationRate.x)],
                                @"beta" : [NSNumber numberWithDouble:rad2deg(rotationRate.y)],
                                @"gamma": [NSNumber numberWithDouble:rad2deg(rotationRate.z)],
                            },
                            @"orientation": @{
                                @"alpha": [NSNumber numberWithDouble:alpha],
                                @"beta" : [NSNumber numberWithDouble:beta],
                                @"gamma": [NSNumber numberWithDouble:gamma],
                                @"heading": [NSNumber numberWithDouble:heading],
                            }
                        };
                        
                        [self storeMotionEventData:result];
                    }];
                }
            }
            else if ([motionManager isAccelerometerAvailable] == YES) {
                motionManager.accelerometerUpdateInterval = period;
                if ([motionManager isAccelerometerActive] == NO) {
                    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue]
                                                        withHandler:^(CMAccelerometerData *accelerometerData, NSError *error)
                     {
                        CMAcceleration accel = accelerometerData.acceleration;
                        NSDictionary* result = @{
                            @"accelerationIncludingGravity": @{
                                @"x": [NSNumber numberWithDouble:accel.x * kGravity],
                                @"y": [NSNumber numberWithDouble:accel.y * kGravity],
                                @"z": [NSNumber numberWithDouble:accel.z * kGravity],
                            }
                        };
                        
                        [self storeMotionEventData:result];
                    }];
                }
            }
            
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT];
            [pluginResult setKeepCallbackAsBool:YES];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                               messageAsString:@"Already started."];
    }
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Motion is not supported by this device."];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

/**
 * Stops device-motion services.
 *
 * The Cordova plugin will return TRUE on success, FALSE if already stopped
 * or an error message if motion is not supported by this device.
 *
 * @param command the Cordova plugin request.
 */
- (void)stop:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    if (motionManager) {
        if (isListening) {
            BOOL empty = [self unregisterListener:command];
            if (empty) {
                isListening = NO;
                if (motionManager.isGyroActive == YES) [motionManager stopGyroUpdates];
                if (motionManager.isAccelerometerActive == YES) [motionManager stopAccelerometerUpdates];
                if (motionManager.isDeviceMotionActive == YES) [motionManager stopDeviceMotionUpdates];
                if (isHeadingAvailable == YES) [locationManager stopUpdatingHeading];
            }
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:YES];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:NO];
    }
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Motion is not supported by this device."];
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
