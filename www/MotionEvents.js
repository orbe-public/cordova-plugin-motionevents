// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var utils = require('cordova/utils');
var argscheck = require('cordova/argscheck');
var services = require('./motionServices');
var MotionData = require('./MotionEventsData');



/**
 * Provides information about the physical orientation and movement of the device
 * @class
 *
 * @prop {boolean} isAvailable — Indicates whether device-motion is available.
 * @prop {boolean} isMotionAvailable — Indicates whether the device-motion service is available.
 * @prop {boolean} isGyroscopeAvailable — Indicates whether a gyroscope is available.
 * @prop {boolean} isAccelerometerAvailable — Indicates whether an accelerometer is available.
 */
var MotionEvents = function () {
    this.uuid = utils.createUUID();
    this.isAvailable = services.isMotionAvailable || services.isAccelerometerAvailable;
    this.isMotionAvailable = services.isMotionAvailable;
    this.isGyroscopeAvailable = services.isGyroscopeAvailable;
    this.isAccelerometerAvailable = services.isAccelerometerAvailable;
};

/**
 * Starts device-motion listening.
 *
 * Options may contain the following keys:
 *  - frequency, the interval for providing device-motion updates in milliseconds (default 100)
 *
 * @param {MotionEvents~handler} callback - The callback that handles the device-motion data.
 * @param {MotionEvents~error} onError - The callback function to be called on error.
 * @param {MotionEvents~options} options — The options for getting the device-motion data. (optional)
 */
MotionEvents.prototype.start = function (callback, onError, options) {
    argscheck.checkArgs('fFO', 'MotionEvents.start', arguments);
    var onResult = function (result) { callback(new MotionData(result)); }
    cordova.exec(onResult, onError, "MotionEvents", "start", [this.uuid, options]);
},

/**
 * Stops device-motion listening.
 *
 * @param {MotionEvents~success} onSuccess - The callback function to be called on success.
 * @param {MotionEvents~error} onError - The callback function to be called on error.
 */
MotionEvents.prototype.stop = function (onSuccess, onError) {
    argscheck.checkArgs('fF', 'MotionEvents.stop', arguments);
    cordova.exec(onSuccess, onError, "MotionEvents", "stop", [this.uuid]);
},

/**
 * The callback to execute when an action succeeded.
 *
 * @callback MotionEvents~success
 * @param {boolean} result — Whether the corresponding native action has been successfully executed
 * or already done.
 */

/**
 * The callback to execute when an action failed.
 *
 * @callback MotionEvents~error
 * @param {string} response — The error message.
 */

/**
 * The function to be invoked with each update to handle new device-motion data.
 *
 * The device-motion data contains the following keys and objects:
 *  - acceleration { x, y, z },
 *    the acceleration of the device on the three axis X, Y and Z, expressed in m/s² where:
 *      - local, representing the acceleration relative to the device's reference frame
 *        (left-right/forward-backward).
 *      - world, representing the acceleration relative to the earth's reference frame
 *        (north-south/east-west).
 *  - accelerationIncludingGravity { x, y, z },
 *    the acceleration of the device on the three axis X, Y and Z with the effect of gravity,
 *    expressed in m/s² where:
 *        - local, representing the acceleration including gravity relative to the device's 
 *          reference frame (left-right/forward-backward).
 *        - world, representing the acceleration including gravity relative to the earth's reference
 *          frame (north-south/east-west).
 *  - rotationRate: { alpha, beta, gamma },
 *    the rate of change of the device's orientation on the three orientation axis alpha, beta and
 *    gamma, expressed in degrees per seconds.
 *  - orientation: { alpha, beta, gamma, heading },
 *    the physical orientation of the device where: 
 *      - alpha, representing the motion of the device around the z axis, express in degrees with
 *        values ranging from 0 (inclusive) to 360 (exclusive).
 *      - beta, representing the motion of the device around the x axis, express in degrees with
 *        values ranging from -180 (inclusive) to 180 (exclusive). This represents a front to back
 *        motion of the device.
 *      - gamma, representing the motion of the device around the y axis, express in degrees with
 *        values ranging from -90 (inclusive) to 90 (exclusive). This represents a left to right
 *        motion of the device.
 *      - heading, representing the difference between the motion of the device around the z axis of
 *        the world system and the direction of the north, express in degrees with values ranging
 *        from 0 to 360, or -1 if heading is not available.
 *      - timestamp, representing the date of the device-motion data
 *
 * @callback MotionEvents~handler
 * @param {Object} data — the device-motion data.
 */

/**
 * The device-motion service options contains the following keys:
 *
 *  - (Number) period, the interval in milliseconds for providing device-motion updates
 *                     default 16.6666ms (or 60Hz).
 *
 * @argument MotionEvents~options
 * @param {Object} options — The device-motion service option.
 */

module.exports = MotionEvents;