// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var argscheck = require('cordova/argscheck');
var channel = require('cordova/channel');

channel.createSticky('onMotionEventsServicesReady');
channel.waitForInitialization('onMotionEventsServicesReady');

/**
 * Provides the status of each motion service.
 * @class
 *
 * @prop {boolean} available — Whether the MotionEventsServices is available.
 * @prop {boolean} isMotionAvailable — Indicates whether the device-motion service is available.
 * @prop {boolean} isGyroscopeAvailable — Indicates whether a gyroscope is available.
 * @prop {boolean} isAccelerometerAvailable — Indicates whether an accelerometer is available.
 */
function MotionEventsServices() {
    this.available = false;
    this.isMotionAvailable = false;
    this.isGyroscopeAvailable = false;
    this.isAccelerometerAvailable = false;

    var me = this;
    channel.onCordovaReady.subscribe(function () {
        me.getAvailability(
            function (services) {
                me.available = true;
                me.isMotionAvailable = services.motion;
                me.isGyroscopeAvailable = services.gyroscope;
                me.isAccelerometerAvailable = services.accelerometer;
                channel.onMotionEventsServicesReady.fire();
            },
            function (error) {
                me.available = false;
                console.warn('Initializing cordova-plugin-motionevents: ' + error);
                channel.onMotionEventsServicesReady.fire();
            }
        );
    });
}

/**
 * Get the status of each motion service.
 *
 * @param {MotionEventsServices~handler} onSuccess — The function to call when all services status
 * are available.
 * @param {Function} onError — The function to call when there is an error getting the services
 * status. (OPTIONAL)
 */
MotionEventsServices.prototype.getAvailability = function (onSuccess, onError) {
    argscheck.checkArgs('fF', 'MotionEventsServices.getAvailability', arguments);
    cordova.exec(onSuccess, onError, "MotionEvents", "getServicesAvailability", []);
};

/**
 * The function to be invoked when all services status are available.
 *
 * The response contains the following keys:
 *  - motion, whether the device-motion service is available.
 *  - accelerometer, whether an accelerometer is available.
 *  - gyroscope, whether a gyroscope is available.
 *
 * @callback MotionEventsServices~handler
 * @param {Object} response — the services status.
 */

module.exports = new MotionEventsServices();