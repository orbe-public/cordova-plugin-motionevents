// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/**
 * The device-motion data.
 * @class
 *
 * @prop {object} acceleration — The acceleration of the device on the three axis X, Y and Z,
 * expressed in m/s².
 * @prop {object} accelerationIncludingGravity — The acceleration of the device on the three axis
 * X, Y and Z with the effect of gravity, expressed in m/s².
 * @prop {object} rotationRate — The rate of change of the device's orientation on the three
 * orientation axis alpha, beta and gamma, expressed in degrees per seconds.
 * The physical orientation of the device where:
 * @prop {object} orientation.alpha — The motion of the device around the z axis, express in degrees
 * with values ranging from 0 (inclusive) to 360 (exclusive).
 * @prop {object} orientation.beta — The motion of the device around the x axis, express in degrees
 * with values ranging from -180 (inclusive) to 180 (exclusive). This represents a front to back
 * motion of the device.
 * @prop {object} orientation.gamma — The motion of the device around the y axis, express in degrees
 * with values ranging from -90 (inclusive) to 90 (exclusive). This represents a left to right
 * motion of the device.
 * @prop {object} orientation.heading — The difference between the motion of the device around the
 * z axis of the world system and the direction of the north, express in degrees with values ranging
 * from 0 to 360, or -1 if heading is not available.
 * @prop {number} orientation.timestamp — The date of the device-motion data
 */
var MotionEventsData = function (data) {
    this.motion = {
        acceleration: data.acceleration || { local: { x: 0, y: 0, z: 0 }, world: { x: 0, y: 0, z: 0 }, },
        accelerationIncludingGravity: data.accelerationIncludingGravity || { local: { x: 0, y: 0, z: 0 }, world: { x: 0, y: 0, z: 0 }, },
        rotationRate: data.rotationRate || { alpha: 0, beta: 0, gamma: 0 }
    };
    this.orientation = data.orientation || { alpha: 0, beta: 0, gamma: 0, heading: -1 };
    this.timestamp = data.timestamp || new Date().getTime();
};

module.exports = MotionEventsData;