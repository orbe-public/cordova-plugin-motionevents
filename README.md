---
title: Motion Events
description: Device motion and orientation events for Cordova.
---
<!--
The MIT License (MIT)
Copyright © 2021 Orbe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
# Cordova Motion Events Plugin

Access to the built-in accelerometer, gyroscope, and compass in mobile devices and provides measurements of orientation, rotation rate, and acceleration of the device.


## Installation
```
cordova plugin add https://gitlab.com/orbe-public/cordova-plugin-motionevents
```

## Supported Platforms
* IOS


## Features

* Start and stop the services that report movement detected by the device's onboard sensors.
* Retrieves motion-related attributes such as the user-initiated acceleration, rotation rates, orientation relative to calibrated magnetic fields, and orientation relative to gravity.

## Methods

### start
Starts device-motion listening.   
The plugin will return an object representing the current device-motion on success or an error message on failure.

#### Parameters
`handler`, the callback to be invoked with each update to handle new device-motion data: `function(data)`.   
`errorCallback`, the callback to execute on failure: `function(message)`.   
`options`, the options for getting the device-motion data (optional): `object`.  

Options may contain the following keys:
 * period, the interval in milliseconds for providing device-motion updates, default 16.6666ms (or 60Hz).

### stop
Stops device-motion listening.   
The plugin will return `true` on success, `false` if already stopped or an error message if motion is not supported by the device.

#### Parameters
 `successCallback`, the callback to execute on success: `function(status)`.   
 `errorCallback`, the callback to execute on failure: `function(message)`.


## About device-motion data

The device-motion data contains the following keys and objects:
* acceleration { x, y, z }, the acceleration of the device on the three axis X, Y and Z, expressed in m/s² where:
  - local, representing the acceleration relative to the device's reference frame (left-right/forward-backward).
  - world, representing the acceleration relative to the earth's reference frame (north-south/east-west).
* accelerationIncludingGravity { x, y, z }, the acceleration of the device on the three axis X, Y and Z with the effect of gravity, expressed in m/s² where:
  - local, representing the acceleration including gravity relative to the device's reference frame (left-right/forward-backward).
  - world, representing the acceleration including gravity relative to the earth's reference frame (north-south/east-west).
* rotationRate: { alpha, beta, gamma }, the rate of change of the device's orientation on the three orientation axis alpha, beta and gamma, expressed in degrees per seconds.
* orientation: { alpha, beta, gamma, heading }, the physical orientation of the device where: 
  - alpha, representing the motion of the device around the z axis, express in degrees with values ranging from 0 (inclusive) to 360 (exclusive).
  - beta, representing the motion of the device around the x axis, express in degrees with values ranging from -180 (inclusive) to 180 (exclusive). This represents a front to back motion of the device.
  - gamma, representing the motion of the device around the y axis, express in degrees with values ranging from -90 (inclusive) to 90 (exclusive). This represents a left to right motion of the device.
  - heading, representing the difference between the motion of the device around the z axis of the world system and the direction of the north, express in degrees with values ranging from 0 to 360, or -1 if heading is not available.
  - timestamp, representing the date of the device-motion data

## Example

```js
var motion = new MotionEvents();
motion.start(
    function (data) {
        // do something with data
    },
    function (err) {
        console.error(err);
    },
    { period: 500 }
);

...

motion.stop(
    function (status) {
        console.log(status ? 'Stop successfully' : 'Already stopped');
    },
    function (err) {
        console.error(err);
    }
);
```

You might want to check services availability before listening to device-motion data.  
If the gyroscope service is not available, the returned device-motion data will only containt accelerometer values, other data values will be setted with their default values (0) or -1 for the *orientation.heading* value.

```js
var motion = new MotionEvents();

console.log("MotionEvents is available", motion.isAvailable);
console.log("Device motion service is available", motion.isMotionAvailable);
console.log("Accelerometer service is available", motion.isAccelerometerAvailable);
console.log("Gyroscope service is available", motion.isGyroscopeAvailable);

if (motion.isAvailable) (
    motion.start(
        function (data) {
            // do something with data
        },
    );
);
```

## Requirements

iOS 9.0 and later.
